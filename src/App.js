import React from 'react';
import { useSelector, useDispatch } from 'react-redux'

import Layout from './components/Layout';

import TodoCard from './components/TodoCard';

export default () => {
  const toDoTasks = useSelector(state => state.toDoListReducer.toDoList)
  const doingTasks = useSelector(state => state.doingListReducer.doingList)
  const doneTasks = useSelector(state => state.doneListReducer.doneList)

  return (
    <Layout>
      <TodoCard type='toDo' tasks={toDoTasks}></TodoCard>
      <TodoCard type='doing' tasks={doingTasks}></TodoCard>
      <TodoCard type='done' tasks={doneTasks}></TodoCard>
    </Layout>  
  );
}
