import {
  createStore, compose, applyMiddleware,
} from 'redux';

import rootReducers from './reducers';

const composeEnhancers = typeof window === 'object'
  && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
  }) : compose;

const enhancer = composeEnhancers();

export default createStore(
  rootReducers,
  enhancer,
);

