import { ADD_NEW_DOING_TASK, REMOVE_DOING_TASK } from '../actionTypes';

const initialState = {
  doingList: [],
};
const doingListReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEW_DOING_TASK:
      return { ...state, doingList: [...state.doingList, action.newTask] }
    case REMOVE_DOING_TASK:
      let newState = state.doingList.filter(item => item.id !== action.taskId)
      return { newState }
    default:
      return state;
  }
};

export default doingListReducer;