import { ADD_NEW_DONE_TASK, REMOVE_DONE_TASK } from '../actionTypes';

const initialState = {
  doneList: [],
};
const doneListReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEW_DONE_TASK:
      return { ...state, doneList: [...state.doneList, action.newTask] }
    case REMOVE_DONE_TASK:
      let newState = state.doneList.filter(item => item.id !== action.taskId)
      return { newState }
    default:
      return state;
  }
};

export default doneListReducer;