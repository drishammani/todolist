import { ADD_NEW_TODO_TASK, REMOVE_TODO_TASK } from '../actionTypes';

const initialState = {
  toDoList: [],
};

const toDoListReducer = (state = initialState, action) => {
  console.log(action)
  switch (action.type) {
    case ADD_NEW_TODO_TASK:
      return { ...state, toDoList: [...state.toDoList, action.newTask] }
    case REMOVE_TODO_TASK:
      let newState = state.toDoList.filter(item => item.id !== action.taskId)
      return { newState }
    default:
      return state;
  }
};

export default toDoListReducer;