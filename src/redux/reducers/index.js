import { combineReducers } from 'redux';

import toDoListReducer from './toDoListReducer'
import doingListReducer from './doingListReducer'
import doneListReducer from './doneListReducer'

const rootReducer = combineReducers({
  toDoListReducer,
  doingListReducer,
  doneListReducer
});

export default rootReducer;
