import { 
  ADD_NEW_TODO_TASK,
  REMOVE_TODO_TASK,
  ADD_NEW_DOING_TASK,
  REMOVE_DOING_TASK,
  ADD_NEW_DONE_TASK,
  REMOVE_DONE_TASK
} from '../actionTypes';

export const addNewToDoTask = (newTask) => ({ type: ADD_NEW_TODO_TASK, newTask });
export const removeToDoTask = (taskId) => ({ type: REMOVE_TODO_TASK, taskId });

export const addNewDoingTask = (newTask) => ({ type: ADD_NEW_DOING_TASK, newTask });
export const removeDoingTask = (taskId) => ({ type: REMOVE_DOING_TASK, taskId });

export const addNewDoneTask = (newTask) => ({ type: ADD_NEW_DONE_TASK, newTask });
export const removeDoneTask = (taskId) => ({ type: REMOVE_DONE_TASK, taskId });
