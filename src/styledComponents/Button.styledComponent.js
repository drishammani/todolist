import styled from 'styled-components';

const Button = styled.button`
  height: ${props => props.height ? props.height : '20px'};
  width: ${props => props.width ? props.width : '20px'};
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${props => props.color ? props.color : '#000'};
  font-family: sans-serif;
  border: none;
  font-size: ${props => props.size ? props.size : '15px'};
  outline: 0;
  margin: 5px;
  padding: 5px;

  &:hover {
    cursor: pointer;
    ${({ colorHover }) => colorHover && `
      color: ${colorHover};
    `}
  }
`

export default Button;