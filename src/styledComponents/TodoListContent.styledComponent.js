import styled from 'styled-components';

const TodoListContent = styled.li`
  height: ${props => props.open === true ? '80px' : '50px' };
  width: 100%;
  margin: 0;
  padding: 0;
  background-color: ${props => props.even % 2 !== 0 ? '#f1f2f6' : '#FFF' };
  box-sizing:border-box;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  transition: 1s;
  overflow: hidden;
`

export default TodoListContent;