import styled from 'styled-components';

const Box = styled.div`
  height: ${props => props.height ? props.height : '100%'};
  min-height: ${props => props.minHeight ? props.minHeight : null};
  width: ${props => props.width ? props.width : '100%'};
  overflow: scroll;
  font-family: sans-serif;
  margin: ${props => props.margin ? props.margin : '0'};
  padding: ${props => props.padding ? props.padding : '0'};
  background-color: ${props => props.bg ? props.bg : null};
  display: flex;
  ${({ cc }) => cc && `
    align-items: center;
    justify-content: center;
  `}
  ${({ cl }) => cl && `
    align-items: center;
    justify-content: flex-start;
  `}
  ${({ cr }) => cr && `
    align-items: center;
    justify-content: flex-end;
  `}
  ${({ sb }) => sb && `
    align-items: center;
    justify-content: space-between;
  `}
  ${({ row }) => row && `
    flex-direction: row;
  `}
  ${({ column }) => column && `
    flex-direction: column;
  `}
  ${({ br }) => br && `
    border-right: 1px solid gainsboro;
  `}
  ${({ bl }) => bl && `
    border-left: 1px solid gainsboro;
  `}
  ${({ bt }) => bt && `
    border-top: 1px solid gainsboro;
  `}
  ${({ fixed }) => fixed && `
    position: fixed;
  `}
  ${({ relative }) => relative && `
    position: relative;
  `}
  ${({ absolute }) => absolute && `
    position: absolute;
  `}
  

`

export default Box;