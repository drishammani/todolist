import styled from 'styled-components';

const Input = styled.input`
  height: 50px;
  width: 100%;
  margin-top: 10px;
  padding: 5px 10px 5px 10px;
  background-color: #FFF;
  color: #778ca3;
  font-size: 16px;
  box-sizing: border-box;
  border-radius: 5px;
  border: ${props => props.border ? `1.5px solid ${props.border}` : '0.5px solid gainsboro' };
  outline: 0;
`

export default Input;