import styled from 'styled-components';

const InputBox = styled.div`
  height: ${ props => props.open ? '90px' : '0px' };
  width: 100%;
  margin: 0;
  padding-left: 10px;
  padding-right: 10px;
  background-color: #FFF;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  transition: 0.3s;
  box-sizing: border-box;

`

export default InputBox;