import styled from 'styled-components';

const Todo = styled.div`
  height: auto;
  max-height: 600px;
  width: 400px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  border-radius: 5px;
  border: 1px solid #d1d8e0;
  overflow: hidden;

  @media (max-width: 400px) {
    width: 100%;
    margin-bottom: 30px;
  }
`

export default Todo;