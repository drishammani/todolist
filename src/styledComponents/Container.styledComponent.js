import styled from 'styled-components';

const Container = styled.div`
  height: 100%;
  width: 100%;
  background-color: #d1d8e0;
`

export default Container;