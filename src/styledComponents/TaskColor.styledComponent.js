import styled from 'styled-components';

const TaskColor = styled.div`
  height: 20px;
  width: 20px;
  margin: 5px;
  padding: 0;
  border: 0.5px solid gainsboro;
  background-color: ${props => props.bc ? props.bc : null };
  box-sizing:border-box;
  border-radius: 50px;

  &:hover {
    cursor: pointer;
    border: 2px solid gainsboro;
  }
`

export default TaskColor;