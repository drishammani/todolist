import styled from 'styled-components';

const NewTaskButton = styled.button`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #dfe4ea;
  color: #778ca3;
  font-family: sans-serif;
  border: none;
  font-size: 15px;
  outline: 0;

  &:hover {
    cursor: pointer;
    background-color: #778ca3;
    color: #dfe4ea;
  }
`

export default NewTaskButton;