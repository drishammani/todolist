import styled from 'styled-components';

const Header = styled.div`
  height: 70px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  background-color: #45aaf2;
  z-index: 99;
`

export default Header;