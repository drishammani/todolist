import styled from 'styled-components';

const Title = styled.h1`
  height: auto;
  width: fit-content;
  font-size: ${props => props.size ? props.size : '30px'};
  color: ${props => props.color ? props.color : '#000'};
  font-family: sans-serif;
  padding: 0;
  margin: 0;
`

export default Title;