import styled from 'styled-components';

const TodoList = styled.ul`
  height: auto;
  width: 100%;
  margin: 0;
  padding: 0;
  overflow-x:auto;
  position: relative;
  box-sizing:border-box;
  display: flex:
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  font-family: sans-serif;
`

export default TodoList;