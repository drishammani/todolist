import styled from 'styled-components';

const Content = styled.div`
  height: auto;
  min-height: 100%;
  width: 100%;
  display: flex;
  align-items: flex-start;
  justify-content: space-around;
  position: absolute;
  overflow: auto;
  padding: 100px 20px 20px 20px;
  box-sizing: border-box;

  @media (max-width: 400px) {
    flex-direction: column;
    justify-content: flex-start;
  }

`

export default Content;