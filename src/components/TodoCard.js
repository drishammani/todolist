import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'

import { 
  addNewToDoTask,
  removeToDoTask,
  addNewDoingTask,
  removeDoingTask,
  addNewDoneTask,
  removeDoneTask
} from '../redux/actions/addTaskAction';

import Title from '../styledComponents/Title.styledComponent';
import Todo from '../styledComponents/Todo.styledComponent';
import TodoList from '../styledComponents/TodoList.styledComponent';
import TodoListContent from '../styledComponents/TodoListContent.styledComponent';
import Box from '../styledComponents/Box.styledComponent';
import NewTaskButton from '../styledComponents/NewTaskButton.styledConponent';
import Button from '../styledComponents/Button.styledComponent';
import Input from '../styledComponents/Input.styledComponent';
import InputBox from '../styledComponents/InputBox.styledComponent';
import TaskColor from '../styledComponents/TaskColor.styledComponent';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faCheck } from '@fortawesome/free-solid-svg-icons'

export default ({ type, tasks }) => {
  const [displayInput, setDisplayInput] = useState(false);
  const [value, setValue] = useState(String);
  const [color, setColor] = useState('#45aaf2');
  const [openModal, setOpenModal] = useState(false)
  const [modalIndex, setModalIndex] = useState(Number)
  const dispatch = useDispatch();

  const saveTask = () => {
    const obj = {
      id: Date.now(),
      value: value, 
      status: type, 
      color: color
    }

    if (value && type === 'toDo')
      dispatch(addNewToDoTask(obj))
    else if (value && type === 'doing')
      dispatch(addNewDoingTask(obj))
    else if (value && type === 'done')
      dispatch(addNewDoneTask(obj))

    setDisplayInput(false);
    setValue('');
  }

  const cancelTask = () => {
    setDisplayInput(false);
    setValue('');
  }

  const displayModal = (i) => {
    setModalIndex(i)
    setOpenModal(!openModal)
  }

  const removeTask = (id) => {
    if (type === 'toDo')
      dispatch(removeToDoTask(id))
    else if (type === 'doing')
      dispatch(removeDoingTask(id))
    else if (type === 'done')
      dispatch(removeDoneTask(id))
  } 

  const validateTask = (id) => {
    let task
    tasks.forEach(elem => {
      if (elem.id === id) {
        task = elem
      }
    });
    if (type === 'toDo') {
      dispatch(removeToDoTask(id))
      dispatch(addNewDoingTask(task))
    }
    else if (type === 'doing') {
      dispatch(removeDoingTask(id))
      dispatch(addNewDoneTask(task))
    }
    else if (type === 'done') {
      dispatch(removeDoneTask(id))
    }
  }

  return (
    <Todo>
      <Box height='60px' bg='#26de81' cc>
        <Title size='20px' color='#FFF' top>{type}</Title>
      </Box>
      <TodoList>
        {tasks && tasks.map((task, i) =>
          <TodoListContent key={i} even={i} open={i === modalIndex && openModal}>
            <Box height='50px' minHeight='50px' width='100%' row>
              <Box height='50px' width='10%' cc br>
                {i}
              </Box>
              <Box height='50px' width='90%' cl padding='0 5px'>
                {task.value} {openModal}
              </Box>
              <Box height='50px' width='10%' cc column bl>
                <TaskColor bc={task.color} />
                <div onClick={() => displayModal(i)}>...</div>
              </Box>
            </Box>
            { i === modalIndex &&
              <Box height='30px' width='100%' cr row bt> 
                <Box height='30px' width='30px' cc>
                  <FontAwesomeIcon onClick={() => validateTask(task.id)} icon={faCheck} />
                </Box>
                <Box height='30px' width='30px' cc>
                  <FontAwesomeIcon onClick={() => removeTask(task.id)} icon={faTrash} />
                </Box>
              </Box>
            }
          </TodoListContent>
        )}
      </TodoList>
      <InputBox open={displayInput}>
        <Input
          type="text"
          border={color}
          placeholder="Add new task"
          value={value}
          onChange={e => setValue(e.target.value)}
        />
        <Box height='30px' sb row>
          <Box width='auto' cc row>
            <TaskColor bc='#45aaf2' onClick={() => setColor('#45aaf2')} />
            <TaskColor bc='#26de81' onClick={() => setColor('#26de81')} />
            <TaskColor bc='#fed330' onClick={() => setColor('#fed330')} />
            <TaskColor bc='#fa8231' onClick={() => setColor('#fa8231')} />
            <TaskColor bc='#eb3b5a' onClick={() => setColor('#eb3b5a')} />
          </Box>
          <Box width='auto' cc row>
            <Button height='30px' width='40px' color='#1e90ff' colorHover='#227093' onClick={saveTask}>save</Button>
            <Button height='30px' width='40px' color='#ff4757' colorHover='#b33939' onClick={cancelTask}>cancel</Button>
          </Box>
        </Box>
      </InputBox>
      <Box height='40px' bg='#FFF' cc>
        <NewTaskButton onClick={() => setDisplayInput(!displayInput)}>+Add new task</NewTaskButton>
      </Box>
    </Todo>
  )
}