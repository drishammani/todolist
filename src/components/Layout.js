import React from 'react';

import Header from '../styledComponents/Header.styledComponent';
import Container from '../styledComponents/Container.styledComponent';
import Content from '../styledComponents/Content.styledComponent';
import Title from '../styledComponents/Title.styledComponent';

export default (props) => (
  <Container>
    <Header>
      <Title color="#FFF">Todo List</Title>
    </Header>
    <Content>
      {props.children}
    </Content>
  </Container>
)